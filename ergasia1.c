#include <stdio.h>
#include <stdlib.h> 
#include <time.h> 
#include <unistd.h>
#include <fcntl.h>
#include <semaphore.h> 
#include <sys/wait.h> 
int main()
{
	int N,fd[2];
    printf("Dwse megethos pinaka:");
    scanf("%d", &N);
    int array[N],random[N/2],T[3],temp;
    T[1]=1;

    
  srand (time (NULL)); //Για να εμφανίζει σε κάθε εκτέλεση τυχαία νούμερα
    	for (int i = 0; i < N; i++ ) {
     		array[i]= rand() % (100 + 1 - (-100)) + (-100); //Γεμίζει τον πίνακα με τυχαίους αριθμούς στο διάστημα [-100,100]
     		printf("array=%d,\n",array[i] );
  
         }


    pipe(fd); //δημίουργω pipes ώστε στο ερώτημα της ταξινόμησης να μπορώ να πάρω τα νούμερα που χρειάζομαι απο τα άλλα procceses
    sem_t *sem = sem_open("amazing_semaphore", O_CREAT|O_EXCL, 0, 1); //δημιουργω τον σημαφόρο
  
   	pid_t t1,t2;
    t1 = fork();    //κανει τα fork
    t2 = fork(); 
   

    if(t1<0 || t2<0){
		printf("failed to fork"); //αν τα t είναι αρνητικά έχει αποτύχει το fork
	}


    else if (t1 > 0 && t2 > 0) {
    	close(fd[0]);
        sem_wait(sem);  //κάνει lock την διεργασία
        sleep(1);       //  κάνει sleep την διεργασία
        sem_post(sem);  //κάνει unlock την διεργασία
        for(int i=0; i<N/2; i++){
        random[i]=rand() % ((N-1) + 1 - (0)) + (0); //διαλέγει ενα τυχαίο στοιχείο του πίνακα 
        printf("tuxaia i=%d\n",random[i]);
        array[random[i]]=rand() % (100 + 1 - (-100)) + (-100); //γεμίζει το τυχαίο στοιχείο
        printf("%d\n", array[random[i]]);

        }
    	for(int i=0; i<N; i++){
    		T[0]=T[0]+array[i];
    		
    	}
    	write(fd[1], &T[0], sizeof(T[0]));  // γράφει στο pipe ωστε να το χρησιμοποιήσει αργότερα το νούμερο
    	printf("T1=%d\n",T[0]);
    	close(fd[1]);
    	
    }

    else if(t1 > 0 && t2==0){
    	srand (time (NULL));
    	close(fd[0]);
	sem_wait(sem);
        sleep(1);
        sem_post(sem);
        
        for(int i=0; i<N/2; i++){
        random[i]=rand() % ((N-1) + 1 - (0)) + (0); 
        printf("tuxaia i=%d\n",random[i]);
        array[random[i]]=rand() % (100 + 1 - (-100)) + (-100);
        printf("%d\n", array[random[i]]);

        }
      
        
    	for(int i=0; i<N; i++){
    		T[1]=T[1]*array[i];         //κανει το ίδιο με πριν απλά βρίσκει το γινόμενο 
    	}
    	write(fd[1], &T[1], sizeof(T[1]));
    	printf("T2=%d\n",T[1]);
    	close(fd[1]);
    	
    	
		 
    }
    else if (t1 == 0 && t2 == 0) { 
    	close(fd[0]);
    	
    	T[2]= rand() % (1000 + 1 - (-1000)) + (-1000); //βρίσκει τυχαίο αριθμό[-1000,1000]
    	
    	write(fd[1], &T[2], sizeof(T[2]));
    	printf("T3=%d\n",T[2]);
    	close(fd[1]);
    	
    }
     
    else if (t1 == 0 && t2 > 0) { 
    	
    	close(fd[1]);
    	read(fd[0], &T[0], sizeof(T[0]));   //εδώ διαβαζει τα pipes και παίρνει τις μεταβλητες
    	read(fd[0], &T[1], sizeof(T[1]));
      	read(fd[0], &T[2], sizeof(T[2]));
      	 
        for (int i = 0; i <3; i++) { 
  
            for (int j = 0; j < 2; j++) { 
  
                if (T[j] < T[j + 1]) {      //κάνω bubblesort για να τα ταξινομίσω σε φθίνουσα σειρά
  
                    temp = T[j]; 
                    T[j] = T[j + 1]; 
                    T[j + 1] = temp; 
                } 
            } 
        }
        printf("sorted:\n");
        for (int i = 0; i < 3; ++i)
        {
        	printf("%d\n",T[i]);
        }
    	close(fd[0]);
		sem_unlink("amazing_semaphore");
 		 
	}
   return 0;
}
